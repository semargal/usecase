import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='usecase',
    version='0.1',
    author="Serhii Kupriianov",
    author_email="semargal@gmail.com",
    description="A domain logic wrapper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/semargal/usecase",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    install_requires=[
        "pydantic==1.6.1"
    ]
)
