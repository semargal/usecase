"""
This package provides a way to follow SOLID principles in Python applications
by holding all business (domain) logic in the services.

It is completely web framework agnostic and only provides
validation and encourages using composition and extension
over inheritance and rewriting.
"""
from .services import *

__all__ = ('UseCase', 'PlaneService', 'ModelService', 'ServiceWrapper')

UseCase = ModelService
